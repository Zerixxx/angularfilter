

  Array.prototype.unique = function(comparator){

      for(var i = 0; i < this.length; i++){
          for(var j = i+1; j < this.length; j++){
              if(comparator(this[i],this[j])){
                  this.splice(j--, 1); --i;
              }
          }
      }
      return this;
  };

  Array.prototype.merge = function(arr, comparator){
      var result = [];
      if(!this.length)  return arr;

      for(var i = 0; i < this.length; i++){
          for(var j = 0; j < arr.length; j++){
              if(comparator(this[i],arr[j])){
                  result.push(this[i]);
                  arr.splice(j, 1);
                  break;
              }
          }
      }
      return result;
  };

  String.prototype.replaceAll = function(search, replacement){
      return this.split(search).join(replacement);
  };


app.filter('full', function($filter){
	
  function preparePattern(value, rules){
  		var pattern = '',
          rules = rules || [];

  		if(value == null || value == '')   
          return new RegExp('^.*$', 'i', 'g');

      for(var i = 0; i < rules.length; i++){
          pattern = value.replaceAll(rules[i][0], rules[i][1]);
      }

      // not strict search
      if(rules != null)
          return new RegExp('^.*' + pattern + '.*$', 'i', 'g');

      return new RegExp('^' + pattern + '$', 'i', 'g');
  }

  function filterByRegex(items, property, pattern){
      var filtered = [];
      for(var i = 0; i < items.length; i++){
          if(items[i].hasOwnProperty(property) && items[i][property] != null){
              if(pattern.test(items[i][property].toString())){
                  filtered.push(items[i]);
              }
          }
      }
      return filtered;
  }

  
	return function(items, filter, strict, rules, separator = ';'){
  		var filtered = [],
          pattern,
          values,
          strict = strict || [],
          rules = rules || [['*','.*']],
          tmpBuffer,
          compareRules; 
      
      if(!angular.isObject(filter) || 
         !Object.getOwnPropertyNames(filter).length)	
          return items;
      
      for(var property in filter){
          tmpBuffer = [];

          if(!angular.isString(filter[property]))  
              continue;

          if(property === '$') {
              tmpBuffer = $filter('filter')(items,filter.$);
          }else{
              values = filter[property].split(separator);

              for(var i = 0; i < values.length; i++){
                  values[i] = values[i].replace(/[^a-zA-Z0-9]/g, '');
                  compareRules = strict.indexOf(property) == -1 ? rules : null;
                  pattern = preparePattern(values[i], compareRules);
                  tmpBuffer = tmpBuffer
                    .concat(filterByRegex(items, property, pattern))
                    .unique(function(a,b){ return a.Id === b.Id;});
              }
          }

          filtered = filtered.merge(tmpBuffer, function(a,b){
              return a.Id === b.Id;
          });
      }

      return filtered;
  };
});