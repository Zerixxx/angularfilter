
var data = [
    {
        Id: 0,
        Group: "first-group",
        Status: "healthy",
        Text: "text"
    },
    {
        Id: 1,
        Group: "second-group",
        Status: "faulty",
        Text: "astext sdf"
    },
    {
        Id: 2,
        Group: "third-group",
        Status: "healthy",
        Text: "text drgt"
    },
    {
        Id: 3,
        Group: "fourth-group",
        Status: "faulty",
        Text: "texteryt"
    },
    {
        Id: 4,
        Group: "fifth-group",
        Status: "healthy",
        Text: "textsdf"
    },
    {
        Id: 5,
        Group: "first-group",
        Status: "healthy",
        Text: "teasdxt"
    },
    {
        Id: 6,
        Group: "second-group",
        Status: "healthy",
        Text: "dfgtext"
    },
];